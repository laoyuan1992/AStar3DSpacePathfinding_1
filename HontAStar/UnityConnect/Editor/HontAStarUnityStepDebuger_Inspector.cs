﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Hont.AStar
{
    [CustomEditor(typeof(HontAStarUnityStepDebuger))]
    public class HontAStarUnityStepDebuger_Inspector : Editor
    {
        HontAStarUnityStepDebuger mDebuger;

        Position mCurrentPosition;
        int mLastStep;
        bool mIsInit;


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (mDebuger == null)
                mDebuger = base.target as HontAStarUnityStepDebuger;

            if (mDebuger.astar == null)
                mDebuger.astar = mDebuger.gameObject.GetComponent<HontAStarUnity>();

            if (mDebuger.astar == null) return;

            if (!mIsInit)
            {
                mDebuger.Init();
                mIsInit = true;
            }

            if (mDebuger.StepList != null && mDebuger.StepList.Count > 0)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Pathfinding Step");
                mDebuger.currentStep = EditorGUILayout.IntSlider(mDebuger.currentStep, 0, mDebuger.StepList.Count - 1);

                if (mDebuger.currentStep != mLastStep)
                {
                    var stepInfo = mDebuger.StepList[mDebuger.currentStep];
                    var grid = mDebuger.astar.Grid;
                    mCurrentPosition = stepInfo.AStarPos;

                    foreach (Position item in grid)
                    {
                        var node = grid.GetUserData(item) as HontAStarUnityDebugNode;
                        node.debugSetting.isDebuger = false;
                    }

                    var debugNode = grid.GetUserData(mCurrentPosition) as HontAStarUnityDebugNode;

                    debugNode.debugSetting.isDebuger = true;

                    switch (stepInfo.NodeType)
                    {
                        case DebugerNodeTypeEnum.CompareNode:

                            debugNode.debugSetting.debugerColor = new Color(0, 1, 1, 0.5f);
                            debugNode.debugSetting.debugerText = "F: " + mDebuger.astar.Grid.GetF(debugNode.AStarPosition);

                            break;
                        case DebugerNodeTypeEnum.NextNode:

                            debugNode.debugSetting.debugerColor = new Color(1, 0, 1, 0.5f);
                            debugNode.debugSetting.debugerText = "F: " + mDebuger.astar.Grid.GetF(debugNode.AStarPosition);

                            break;
                        case DebugerNodeTypeEnum.CloseNode:

                            debugNode.debugSetting.debugerColor = Color.gray;

                            break;
                    }

                    mLastStep = mDebuger.currentStep;
                    SceneView.RepaintAll();
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal(GUI.skin.box);
                if (GUILayout.Button("<"))
                {
                    mDebuger.currentStep = Mathf.Max(0, mDebuger.currentStep - 1);
                }
                if (GUILayout.Button(">"))
                {
                    mDebuger.currentStep = Mathf.Min(mDebuger.StepList.Count - 1, mDebuger.currentStep + 1);
                }
                EditorGUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Box("Pos: " + mCurrentPosition);
                GUILayout.Box("F: " + mDebuger.astar.Grid.GetF(mCurrentPosition));
                GUILayout.Box("G: " + mDebuger.astar.Grid.GetG(mCurrentPosition));
                GUILayout.Box("H: " + mDebuger.astar.Grid.GetH(mCurrentPosition));
                GUILayout.EndHorizontal();
            }
        }
    }
}
